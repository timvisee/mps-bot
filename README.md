# mprs-bot
A bot for dns2utf8's [multiplayer_snake] in Rust.

Connects to the snake server over a websocket. It takes control over a random
snake.

The bot listens to the server, and sends a movement vote for the snake each
game tick. It uses some basic path finding techniques to find the _best_ path to
the fruit. It tries to cut of other snakes by moving in front of them when
close. Wrapping the field is taken into account as well to pull of some crazy
moves.

The bot reevaluates the current game state each tick to determine it's best next
move. It does not simulate future game ticks to determine beter moves. So the
bot may cut itself off after a while. This may be a fun thing to improve on some
time.

Configuration is hardcoded in [`src/main.rs`](./src/main.rs).

## License
This project is released under the GNU GPL-3.0 license.
Check out the [LICENSE](LICENSE) file for more information.

[multiplayer_snake]: https://gitlab.com/dns2utf8/multiplayer_snake
