use pathfinding::directed::astar;
use rand::{seq::SliceRandom, thread_rng};
use serde::{Deserialize, Serialize};
use tungstenite::Message;
use url::Url;

const URL: &str = "ws://localhost:8080/ws/";
const RECONNECT_DELAY: u64 = 2;

const THREADS: usize = 1;
const DEBUG: bool = THREADS <= 1;

const CONTROL: bool = true;
const MY_COLOR: &str = "#ff1400";
const ONLY_CONTROL_MY_COLOR: bool = false;
const SABOTAGE_OTHER_COLORS: bool = false;

const PREDICT_LEN: usize = 5;
const PREDICT_TURNS: bool = true;

const COST_EMPTY: u32 = 10;
const COST_OCCUPIED: u32 = 100;
const COST_PREDICT: u32 = 50;
const COST_PREDICT_FUTURE: u32 = 1;

type WS = tungstenite::WebSocket<tungstenite::client::AutoStream>;

fn main() {
    let mut handles = vec![];

    for _ in 0..THREADS {
        handles.push(std::thread::spawn(|| connect_run_loop()));
    }

    for handle in handles {
        if let Err(err) = handle.join() {
            eprintln!("Failed to join thread: {:?}", err);
        }
    }
}

fn connect_run_loop() {
    loop {
        if let Some(mut socket) = connect() {
            run(&mut socket);
            socket.close(None).expect("failed to close socket");
        } else {
            eprintln!("ERR cannot connect");
        }

        eprintln!("Reconnecting in 2 seconds...");
        std::thread::sleep(std::time::Duration::from_secs(RECONNECT_DELAY));
    }
}

fn connect() -> Option<WS> {
    let (socket, response) = tungstenite::connect(Url::parse(URL).unwrap()).ok()?;
    println!("Connected! HTTP {}", response.status());
    Some(socket)
}

fn run(socket: &mut WS) {
    let mut game = Game::default();

    loop {
        let msg = match socket.read_message() {
            Ok(msg) => msg,
            Err(err) => {
                eprintln!("ERR failed to read message from websocket: {:?}", err);
                return;
            }
        };
        let msg_result = handle_json(&msg.to_string(), &mut game, socket);

        if msg_result.is_err() {
            break;
        }
    }
}

#[derive(Debug)]
struct Game {
    pub field: Field,
    pub my_color: String,
    pub me: Option<Snake>,
}

impl Game {
    fn update_snakes(&mut self, snakes: Vec<Snake>) {
        let field = &mut self.field;

        field.clear();

        for snake in snakes {
            let is_me = snake.color == self.my_color;

            let head = snake.body[0];
            let tail = *snake.body.last().unwrap();
            let dist_to_fruit = head.distance_to_wrapped(field.fruit, &field);
            let may_grow = dist_to_fruit <= 1 || snake.body.len() <= 1;

            // Occupy snake except tail
            for point in &snake.body[..snake.body.len() - 1] {
                field.add_cell(*point, Cell::Occup);
            }

            // Predict tail to become longer, if head is close to sweet, or if snake spawns
            if may_grow {
                field.add_cell(tail, Cell::Occup);
            }

            // Occupy cell in front/next to other snakes
            if !is_me {
                let mut head = snake.body[0];
                for i in 0..PREDICT_LEN {
                    head = head.move_one(snake.heading).wrap(field);
                    field.add_cell(
                        head,
                        if i == 0 {
                            Cell::Predict(true)
                        } else {
                            Cell::PredictFuture
                        },
                    );
                }

                if PREDICT_TURNS {
                    let heading = snake.heading;
                    let head = snake.body[0];
                    field.add_cell(
                        head.move_one(heading.rotate(1)).wrap(field),
                        Cell::Predict(false),
                    );
                    field.add_cell(
                        head.move_one(heading.rotate(-1)).wrap(field),
                        Cell::Predict(false),
                    );
                }
            }

            // Remember our snake
            if is_me {
                self.me = Some(snake);
            }
        }

        if DEBUG {
            field.display();
        }
    }

    fn find_path(&self) -> Option<Vec<Point>> {
        // We must know me
        if self.me.is_none() {
            return None;
        }

        let pf = self.field.to_pffield();
        let start = self.me.as_ref().unwrap().body[0];
        let end = self.field.fruit;

        pf.find_path(start, end)
            .map(|path| path.into_iter().map(|p| p.to_point()).collect())
    }

    fn find_path_next_cell(&self) -> Option<Point> {
        match self.find_path() {
            Some(path) if path.len() > 1 => Some(path[1]),
            _ => None,
        }
    }

    fn send_move(&mut self, socket: &mut WS) {
        // We must know me
        if self.me.is_none() {
            eprintln!("ERR cannot move we don't know me");
            return;
        }

        let me = self.me.as_ref().unwrap();
        let heading = me.heading;
        let at = me.body[0];

        // Build shuffled list of possible moves (heading, point)
        let mut options: Vec<(Dir, Point)> = vec![
            (heading, at.move_one(heading).wrap(&self.field)),
            (
                heading.rotate(1),
                at.move_one(heading.rotate(1)).wrap(&self.field),
            ),
            (
                heading.rotate(-1),
                at.move_one(heading.rotate(-1)).wrap(&self.field),
            ),
        ];
        options.shuffle(&mut thread_rng());

        // Find next best cell with path finding, move it in front of move options list
        if let Some(target) = self.find_path_next_cell() {
            let rel = at.rel_to_wrap(target, &self.field);
            let target_dir = rel.direction();

            let i = options.iter().position(|(dir, _)| dir == &target_dir);
            if let Some(i) = i {
                options.rotate_left(i);
            }
        } else {
            eprintln!("ERR no path found, picking random");
        }

        // Move cells to avoid the most to the back of options list
        options.sort_by_key(|(_, a)| {
            self.field
                .cell(*a)
                .map(|c| c.must_avoid_weight())
                .unwrap_or(0)
        });

        if options.is_empty() {
            eprintln!("ERR no move options, not sending anything");
            return;
        }

        let is_my_color = self
            .me
            .as_ref()
            .map(|s| s.color == MY_COLOR)
            .unwrap_or(false);

        // Select proper input based on first move
        let input = if !SABOTAGE_OTHER_COLORS || is_my_color {
            options[0]
        } else {
            *options.last().unwrap()
        };
        let input = heading.input_to(input.0);

        if DEBUG {
            // eprintln!("Move rel: {:?}", rel);
            // eprintln!("Target dir: {:?}", target_dir);
            eprintln!("Voting move: {:?}", input);
        }

        // Send move left command
        if CONTROL {
            let cmd_left = Msg::PlayerInput(input);
            send_cmd(socket, cmd_left);
        }
    }
}

impl Default for Game {
    fn default() -> Self {
        Game {
            field: Field::default(),
            my_color: String::new(),
            me: None,
        }
    }
}

fn send_cmd(socket: &mut WS, msg: Msg) {
    let cmd: String = serde_json::to_string(&msg).unwrap();

    if let Err(err) = socket.write_message(Message::Text(cmd)) {
        eprintln!("ERR failed to send command: {:?}", err);
    }
}

fn handle_json(msg: &str, game: &mut Game, socket: &mut WS) -> Result<(), ()> {
    // Handle common weird message
    if msg == "B" {
        return Ok(());
    }

    match serde_json::from_str(msg) {
        Ok(msg) => handle(msg, game, socket),
        Err(err) => {
            eprintln!("ERR unknown msg: {:?}", err);
            eprintln!("  Msg: {}", msg);
            eprintln!("  Error: {:?}", err);
            Ok(())
        }
    }
}

fn handle(msg: Msg, game: &mut Game, socket: &mut WS) -> Result<(), ()> {
    match msg {
        Msg::PingTime { ping } if DEBUG => println!("> Ping: {}ms", ping.ms),
        Msg::UpdatePlayerCount(count) if DEBUG => println!("> Players: {}", count),

        Msg::UpdateFieldInfo(info) => {
            game.field.resize(info.width, info.height);
            game.field.fruit = info.next_fruit;
        }
        Msg::UpdateSnakes(snakes) => {
            game.update_snakes(snakes);
            game.send_move(socket);
        }

        Msg::UpdateControllColor(color) => {
            if ONLY_CONTROL_MY_COLOR && color != MY_COLOR {
                return Err(());
            }

            println!("Controlling player: {}", color);

            game.my_color = color;
        }

        _ => {}
    }

    Ok(())
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
enum Cell {
    Empty,

    /// Occupied by snake body
    Occup,

    /// Predicted to be occupied next round.
    ///
    /// Bool defines whether it is likely to be occupied next round or not.
    Predict(bool),

    PredictFuture,
}

impl Cell {
    fn cost(&self) -> u32 {
        match self {
            Cell::Empty => COST_EMPTY,
            Cell::Occup => COST_OCCUPIED,
            Cell::Predict(_) => COST_PREDICT,
            Cell::PredictFuture => COST_PREDICT_FUTURE,
        }
    }

    fn must_avoid_weight(&self) -> u8 {
        match self {
            Cell::Empty | Cell::PredictFuture => 0,
            Cell::Predict(false) => 1,
            Cell::Predict(true) => 2,
            Cell::Occup => 3,
        }
    }

    /// Return most significant cell state.
    fn most_significant(self, other: Cell) -> Cell {
        // Always overwrite empty
        if self == Cell::Empty {
            return other;
        } else if other == Cell::Empty {
            return self;
        }

        if self.cost() > other.cost() {
            self
        } else {
            other
        }
    }
}

#[derive(Debug)]
struct Field {
    width: usize,
    height: usize,
    cells: Vec<Cell>,
    fruit: Point,
}

impl Field {
    pub fn resize(&mut self, width: usize, height: usize) {
        self.cells = vec![Cell::Empty; width * height];
        self.width = width;
        self.height = height;
    }

    pub fn clear(&mut self) {
        for cell in self.cells.iter_mut() {
            *cell = Cell::Empty;
        }
    }

    pub fn coord(&self, point: Point) -> Option<usize> {
        if point.x >= 0
            && point.y >= 0
            && (point.x as usize) < self.width
            && (point.y as usize) < self.height
        {
            Some((point.y as usize * self.width) + point.x as usize)
        } else {
            None
        }
    }

    fn cell(&self, point: Point) -> Option<Cell> {
        self.coord(point).map(|i| self.cells[i])
    }

    pub fn add_cell(&mut self, point: Point, state: Cell) {
        if let Some(i) = self.coord(point) {
            self.cells[i] = self.cells[i].most_significant(state);
        }
    }

    pub fn display(&self) {
        println!();

        let fruit = self.coord(self.fruit).unwrap();

        for (i, cell) in self.cells.iter().enumerate() {
            match *cell {
                Cell::Occup => eprint!("#"),
                _ if i == fruit => eprint!("o"),
                Cell::Predict(true) => eprint!("X"),
                Cell::Predict(false) => eprint!("x"),
                Cell::PredictFuture => eprint!("?"),
                Cell::Empty => eprint!("."),
            }

            if (i + 1) % self.width == 0 {
                eprintln!();
            }
        }
    }

    pub fn to_pffield(&self) -> PfField {
        PfField::new(self.width as u32, self.height as u32, self.cells.clone())
    }
}

impl Default for Field {
    fn default() -> Self {
        Field {
            width: 0,
            height: 0,
            cells: vec![],
            fruit: Point::at(0, 0),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
enum Msg {
    PlayerInput(Input),
    PingTime {
        #[serde(rename = "Ping")]
        ping: Ping,
    },
    UpdateSnakes(Vec<Snake>),
    UpdateFieldInfo(FieldInfo),
    UpdatePlayerCount(usize),
    UpdateControllColor(String), // #000000 or rgb(0, 0, 0)
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
enum Input {
    Straight,
    Left,
    Right,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct FieldInfo {
    width: usize,
    height: usize,
    next_fruit: Point,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Snake {
    heading: Dir,
    color: String,
    body: Vec<Point>,
}

#[derive(Eq, PartialEq, Hash, Debug, Copy, Clone, Serialize, Deserialize)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    #[must_use]
    fn wrap(&self, field: &Field) -> Point {
        Point::at(
            self.x.rem_euclid(field.width as i32),
            self.y.rem_euclid(field.height as i32),
        )
    }

    #[must_use]
    fn at(x: i32, y: i32) -> Point {
        Point { x, y }
    }

    fn to(&self, other: Point) -> Point {
        Point::at(other.x - self.x, other.y - self.y)
    }

    /// Shorted relative path to target, wraps.
    ///
    /// Returned point will be outside field bounds if wrapped. Use `wrap` to convert to point
    /// inside field.
    fn rel_to_wrap(&self, other: Point, field: &Field) -> Point {
        let mut diffx = other.x - self.x;
        let mut diffy = other.y - self.y;

        if (diffx - field.width as i32).abs() < diffx.abs() {
            diffx = diffx - field.width as i32;
        } else if (diffx + field.width as i32).abs() < diffx.abs() {
            diffx = diffx + field.width as i32;
        }
        if (diffy - field.height as i32).abs() < diffy.abs() {
            diffy = diffy - field.height as i32;
        } else if (diffy + field.height as i32).abs() < diffy.abs() {
            diffy = diffy + field.height as i32;
        }

        Point::at(diffx, diffy)
    }

    /// Manhattan distance to origin.
    ///
    /// Takes the ability to wrap the `field` into account.
    fn distance_wrapped(&self, field: &Field) -> usize {
        let width = field.width as i32;
        let height = field.height as i32;

        let diffx = self.x.abs();
        let diffy = self.y.abs();

        let diff = diffx.min(width - diffx) + diffy.min(height - diffy);

        diff as usize
    }

    /// Manhattan distance to other.
    ///
    /// Takes the ability to wrap the `field` into account.
    fn distance_to_wrapped(&self, other: Point, field: &Field) -> usize {
        self.to(other).distance_wrapped(field)
    }

    #[must_use]
    fn direction(&self) -> Dir {
        if self.x.abs() > self.y.abs() {
            if self.x >= 0 {
                Dir::East
            } else {
                Dir::West
            }
        } else {
            if self.y >= 0 {
                Dir::South
            } else {
                Dir::North
            }
        }
    }

    #[must_use]
    fn move_one(&self, dir: Dir) -> Point {
        match dir {
            Dir::North => Point::at(self.x, self.y - 1),
            Dir::East => Point::at(self.x + 1, self.y),
            Dir::South => Point::at(self.x, self.y + 1),
            Dir::West => Point::at(self.x - 1, self.y),
        }
    }
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
struct Ping {
    ms: usize,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
enum Dir {
    North,
    East,
    South,
    West,
}

impl Dir {
    fn from_num(&self, dir: i32) -> Dir {
        match dir.rem_euclid(4) {
            0 => Dir::North,
            1 => Dir::East,
            2 => Dir::South,
            3 => Dir::West,
            _ => unreachable!(),
        }
    }

    fn num(&self) -> i32 {
        match self {
            Dir::North => 0,
            Dir::East => 1,
            Dir::South => 2,
            Dir::West => 3,
        }
    }

    fn rotate(&self, right: i32) -> Dir {
        self.from_num(self.num() + right)
    }

    fn input_to(&self, other: Dir) -> Input {
        let a = self.num() as i8;
        let b = other.num() as i8;
        let diff = (b - a + 4) % 4;
        match diff {
            0 => Input::Straight,
            1 => Input::Right,
            2 | 3 => Input::Left,
            _ => unreachable!(),
        }
    }
}

#[derive(Debug)]
struct PfField {
    size: PfPos,
    cells: Vec<Cell>,
}

impl PfField {
    pub fn new(width: u32, height: u32, cells: Vec<Cell>) -> Self {
        assert_eq!(width as usize * height as usize, cells.len());
        Self {
            size: PfPos(width as i32, height as i32),
            cells,
        }
    }

    fn cell_value(&self, x: i32, y: i32) -> u32 {
        match self.coord(x, y) {
            Some(i) => self.cells[i],
            None => Cell::Empty,
        }
        .cost()
    }

    pub fn coord(&self, x: i32, y: i32) -> Option<usize> {
        if x >= 0 && y >= 0 && x < self.size.0 && y < self.size.1 {
            Some((y as usize * self.size.0 as usize) + x as usize)
        } else {
            None
        }
    }

    pub fn find_path(&self, start: Point, end: Point) -> Option<Vec<PfPos>> {
        let start = PfPos::from_point(start);
        let end = PfPos::from_point(end);

        let path = astar::astar(
            &start,
            |p| p.successors(&self),
            |p| p.distance(end, &self) as u32 * COST_EMPTY,
            |p| *p == end,
        );

        path.map(|p| p.0)
    }
}

#[derive(Eq, PartialEq, Hash, Debug, Copy, Clone)]
struct PfPos(i32, i32);

impl PfPos {
    fn from_point(point: Point) -> Self {
        Self(point.x, point.y)
    }

    fn successors(&self, field: &PfField) -> Vec<(PfPos, u32)> {
        let rel = [(1, 0), (-1, 0), (0, 1), (0, -1)];

        rel.iter()
            .map(|(x, y)| (self.0 + x, self.1 + y))
            .map(|(x, y)| (x.rem_euclid(field.size.0), y.rem_euclid(field.size.1)))
            // .filter(|(x, y)| *x >= 0 && *y >= 0 && *x < field.size.0 && *y < field.size.1)
            .map(|(x, y)| (PfPos(x, y), field.cell_value(x, y)))
            .collect()
    }

    /// Manhattan distance to other, wrapping around.
    fn distance(&self, other: PfPos, field: &PfField) -> usize {
        let width = field.size.0;
        let height = field.size.1;

        let diffx = (other.0 - self.0).abs();
        let diffy = (other.1 - self.1).abs();

        let diff = diffx.min(width - diffx) + diffy.min(height - diffy);

        diff as usize
    }

    fn to_point(&self) -> Point {
        Point::at(self.0, self.1)
    }
}
